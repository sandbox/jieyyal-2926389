<?php

/**
 * @file
 * Admin settings functions for sfmc_fuelsdk.
 */

/**
 * Admin setting form for sfmc_fuelsdk module.
 */
function _sfmc_fuelsdk_admin_form($form, &$form_state) {
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('SFMC Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['sfmc_fuelsdk_token_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Token Endpoint'),
    '#default_value' => variable_get('sfmc_fuelsdk_token_endpoint'),
  );
  $form['settings']['sfmc_fuelsdk_token_expire'] = array(
    '#type' => 'textfield',
    '#title' => t('Token Expire'),
    '#element_validate' => array('element_validate_number', 'element_validate_max_number'),
    '#description' => t('Token will be expired in this time'),
    '#default_value' => variable_get('sfmc_fuelsdk_token_expire'),
  );
  $form['settings']['sfmc_fuelsdk_clientid'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#default_value' => variable_get('sfmc_fuelsdk_clientid'),
  );
  $form['settings']['sfmc_fuelsdk_clientsecret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client Secret'),
    '#default_value' => variable_get('sfmc_fuelsdk_clientsecret'),
  );
  return system_settings_form($form);
}

/**
 * Validate max number.
 */
function element_validate_max_number($element, &$form_state) {
  $value = $element['#value'];
  if ($value > SFMC_FUELSDK_TOKEN_MAX_TIME) {
    form_error($element, t('%name must be short than %max_number.', array(
      '%name' => $element['#title'],
      '%max_number' => SFMC_FUELSDK_TOKEN_MAX_TIME,
    )));
  }
}

<?php
/**
 * @file
 * Sample request for SFMC.
 */

/**
 * SFMC FuelSDK Sample form.
 */
function _sfmc_fuelsdk_samples_form($form, &$form_state) {
  $form['samples'] = array(
    '#type' => 'fieldset',
    '#title' => t("SFMC Samples"),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['samples']['get_data'] = array(
    '#type' => 'fieldset',
    '#title' => t("SFMC Get Data Samples"),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['samples']['get_data']['data_extension_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Data Extension Name'),
    '#default_value' => variable_get('sfmc_fuelsdk_samples_data_extension'),
  );
  $form['samples']['get_data']['attr_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Attribute Key'),
    '#default_value' => variable_get('sfmc_fuelsdk_samples_attr_key'),
  );
  $form['samples']['get_data']['attr_val'] = array(
    '#type' => 'textfield',
    '#title' => t('Attribute Value'),
    '#default_value' => variable_get('sfmc_fuelsdk_samples_attr_val'),
  );
  $form['samples']['get_data']['get_data_button'] = array(
    '#type' => 'button',
    '#value' => t('Get Data'),
    '#ajax' => array(
      'callback' => '_sfmc_fuelsdk_samples_get_data',
      'wrapper' => 'sfmc-get-data-placement',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  $form['samples']['get_data']['data_info'] = array(
    '#markup' => "<div id = 'sfmc-get-data-placement'></div>",
  );
  return $form;
}

/**
 * Get data ajax callback.
 */
function _sfmc_fuelsdk_samples_get_data($form, &$form_state) {
  $data_extension = $form_state['values']['data_extension_name'];
  $attr_key = $form_state['values']['attr_key'];
  $attr_val = $form_state['values']['attr_val'];
  variable_set('sfmc_fuelsdk_samples_data_extension', $data_extension);
  variable_set('sfmc_fuelsdk_samples_attr_key', $attr_key);
  variable_set('sfmc_fuelsdk_samples_attr_val', $attr_val);
  // Try to load the library and check if that worked.
  $library = libraries_load('FuelSDK');
  if ($library = libraries_load('FuelSDK') && !empty($library['loaded'])) {
    $myclient = new \FuelSdk\ET_Client(FALSE, FALSE, array(
      "clientid" => variable_get('sfmc_fuelsdk_clientid'),
      "clientsecret" => variable_get('sfmc_fuelsdk_clientsecret'),
      "xmlloc" => drupal_get_path('module', 'sfmc_fuelsdk'). "/ExactTargetWSDL.xml",
    ));

    $getDERows = new \FuelSdk\ET_DataExtension_Row();
    $getDERows->authStub = $myclient;

    $getDERows->props = array("UUID", "EmailAddress", "PointEmailSetting", "RewardEmailSetting", "LoginReminderEmailSetting");
    // $getDERows->Name = $data_extension;
    $getDERows->CustomerKey = $data_extension;
    if (!empty($attr_key) && !empty($attr_val)) {
      $getDERows->filter = array(
        'Property' => $attr_key,
        'SimpleOperator' => 'equals',
        'Value' => $attr_val,
      );
    }
    $getResult = $getDERows->get();
    return "<div id = 'sfmc-get-data-placement'>" . json_encode($getResult->results) . "</div>";
  }
}
